package main

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	ginUtil "gitlab.migoinc.com/migotv/golang/gin/util"
)

var CleanerRoute ginUtil.Info = ginUtil.Info{
	Name: "mds/files",
	Endpoints: []ginUtil.Endpoint{
		{
			Method:   http.MethodPost,
			Version:  1,
			Callback: mdsFilesCleanCheck,
			Path:     "clean-check",
		},
	},
}

type MDSFilesCleanCheckReq struct {
	MID     string  `json:"mid" binding:"required"`
	FileIds []int64 `json:"file_ids" binding:"required"`
}

type MDSFilesCleanCheckResp struct {
	StatusCode int     `json:"status_code"`
	Result     []int64 `json:"result"`
}

type BasicResp struct {
	StatusCode int    `json:"status_code"`
	Result     string `json:"result"`
}

func mdsFilesCleanCheck(c *gin.Context) {
	reqBody := MDSFilesCleanCheckReq{}
	if err := c.BindJSON(&reqBody); err != nil {
		c.JSON(http.StatusBadRequest, BasicResp{
			StatusCode: http.StatusBadRequest,
			Result:     "invalid request body",
		})
		return
	}

	fids := []int64{1, 2, 3}

	c.JSON(http.StatusOK, MDSFilesCleanCheckResp{
		StatusCode: http.StatusOK,
		Result:     fids,
	})
}

var routes []ginUtil.Info = []ginUtil.Info{
	CleanerRoute,
}

func main() {
	router := gin.Default()
	ginUtil.SetRoutes(router, routes)

	svr := &http.Server{
		Addr:    ":4003",
		Handler: router,
	}

	log.Fatal(svr.ListenAndServe())
}
